from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse
from .forms import Forum_Form
from .models import Forum


response={}
# Create your views here.
def index(request):
    forum = Forum.objects.all()
    # forum = Forum.objects.filter(perusahaan=perusahaan)

    paginator = Paginator(forum, 3)
    page = request.GET.get('page')
    try:
        halaman = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        halaman = paginator.page(1)


    response['forum_list'] = halaman
    html = 'perusahaan_lowongan/forum.html'
    response['forum_form'] = Forum_Form
    return render(request, html, response)
    # if 'user_login' in request.session:
    #     return HttpResponseRedirect(reverse('perusahaan-lowongan:show_forum'))
    # else:
    #     return True

def add_forum(request):
    form = Forum_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['lowongan'] = request.POST['description']
        forum = Forum(lowongan=response['lowongan'])
        forum.save()
        return HttpResponseRedirect('/perusahaan-lowongan/')
    else:
        return HttpResponseRedirect('/perusahaan-lowongan/')
