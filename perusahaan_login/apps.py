from django.apps import AppConfig


class PerusahaanLoginConfig(AppConfig):
    name = 'perusahaan_login'
