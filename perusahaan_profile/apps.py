from django.apps import AppConfig


class PerusahaanProfileConfig(AppConfig):
    name = 'perusahaan_profile'
